package com.example.prismajavatest;

import com.example.prismajavatest.entity.Book;
import com.example.prismajavatest.entity.BorrowedBook;
import com.example.prismajavatest.entity.User;
import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FileUtil {
    public static List<Book> readBooksFromCSV() {
        List<Book> books = new ArrayList<>();

        try (CSVReader reader = new CSVReader(new FileReader("src/main/resources/csv-files/books.csv"))) {
            String[] line;
            reader.readNext();//skipping headers
            Integer bookIdCount = 0;
            while ((line = reader.readNext()) != null) {
                if(line.length + 1 == Book.class.getDeclaredFields().length){
                    if(!Arrays.asList(line).contains("")){
                        books.add(Book.createBook(line, bookIdCount));
                        bookIdCount++;
                    }
                }
            }

        }
        catch (IOException | CsvValidationException ioe) {
            ioe.printStackTrace();
        }
        return books;
    }

    public static List<User> readUsersFromCSV() {
        List<User> users = new ArrayList<>();

        try (CSVReader reader = new CSVReader(new FileReader("src/main/resources/csv-files/user.csv"))) {
            String[] line;
            reader.readNext();//skipping headers
            Integer userIdCount = 0;
            while ((line = reader.readNext()) != null) {
                if(line.length + 1 == User.class.getDeclaredFields().length){
                    users.add(User.createUser(line, userIdCount));
                    userIdCount++;
                }
            }

        }
        catch (IOException | CsvValidationException ioe) {
            ioe.printStackTrace();
        }
        return users;
    }

    public static List<BorrowedBook> readBorrowedBooksFromCSV() {
        List<BorrowedBook> borrowedBooks = new ArrayList<>();

        try (CSVReader reader = new CSVReader(new FileReader("src/main/resources/csv-files/borrowed.csv"))) {
            String[] line;
            reader.readNext();//skipping headers
            while ((line = reader.readNext()) != null) {
                if(line.length == BorrowedBook.class.getDeclaredFields().length){
                    borrowedBooks.add(BorrowedBook.createBorrowedBook(line));
                }
            }

        }
        catch (IOException | CsvValidationException ioe) {
            ioe.printStackTrace();
        }
        return borrowedBooks;
    }




}