package com.example.prismajavatest;

import com.example.prismajavatest.entity.Book;
import com.example.prismajavatest.entity.BorrowedBook;
import com.example.prismajavatest.entity.User;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

import java.io.File;
import java.util.List;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class PrismaJavaTestApplication {
	public static void main(String[] args) {
		SpringApplication.run(PrismaJavaTestApplication.class, args);
	}
}
