package com.example.prismajavatest;

import com.example.prismajavatest.entity.Book;
import com.example.prismajavatest.entity.User;

import java.util.List;

public interface DataLayer {
    List<User> getBorrowers();
    List<User> getPassiveUsers();
    List<User> getBorrowersByDate(String dateStr);
    List<Book> getBooksByUserAndDate();
    List<Book> getAvailableBooks();
}
