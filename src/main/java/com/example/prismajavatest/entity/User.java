package com.example.prismajavatest.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@AllArgsConstructor
@Getter
@EqualsAndHashCode
public class User {
    Integer id;
    String lastName;
    String firstName;
    @JsonFormat(pattern = "dd/MM/yyyy")
    LocalDate memberSince;
    @JsonFormat(pattern = "dd/MM/yyyy")
    LocalDate memberUntil;
    String gender;

    public static User createUser(String[] attributes, Integer id) {
        LocalDate untilDate = null;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/d/yyyy");
        if(!attributes[3].equals("")){
            untilDate = LocalDate.parse(attributes[3], formatter);
        }
        return new User(id, attributes[0], attributes[1], LocalDate.parse(attributes[2], formatter), untilDate , attributes[4]);
    }
    public static String getUserLastAndFirstName(User user){
        return user.getLastName() + "," + user.getFirstName();
    }
}
