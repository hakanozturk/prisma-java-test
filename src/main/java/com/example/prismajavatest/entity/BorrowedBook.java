package com.example.prismajavatest.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@AllArgsConstructor
@Getter
@EqualsAndHashCode
public class BorrowedBook {
    String borrower;
    String book;
    @JsonFormat(pattern = "dd/MM/yyyy")
    LocalDate borrowedAt;
    @JsonFormat(pattern = "dd/MM/yyyy")
    LocalDate borrowedUntil;

    public static BorrowedBook createBorrowedBook(String[] attributes){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/d/yyyy");

        return new BorrowedBook(attributes[0], attributes[1], LocalDate.parse(attributes[2], formatter), LocalDate.parse(attributes[3], formatter));
    }
}
