package com.example.prismajavatest.entity;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@AllArgsConstructor
@Getter
@EqualsAndHashCode
public class Book {
    Integer id;
    String title;
    String author;
    String genre;
    String publisher;

    public static Book createBook(String[] attributes, Integer id) {
        return new Book(id, attributes[0], attributes[1], attributes[2], attributes[3]);
    }
}
