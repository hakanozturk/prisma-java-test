package com.example.prismajavatest;

import com.example.prismajavatest.entity.Book;
import com.example.prismajavatest.entity.BorrowedBook;
import com.example.prismajavatest.entity.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.stereotype.Service;

import java.util.List;
@Getter
@AllArgsConstructor
@Service
public class DataUtil {
    private List<Book> books;
    private List<User> users;
    private List<BorrowedBook> borrowedBooks;

    public DataUtil() {
        books = FileUtil.readBooksFromCSV();
        users = FileUtil.readUsersFromCSV();
        borrowedBooks = FileUtil.readBorrowedBooksFromCSV();
    }

}
