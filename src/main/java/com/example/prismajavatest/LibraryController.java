package com.example.prismajavatest;

import com.example.prismajavatest.entity.Book;
import com.example.prismajavatest.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class LibraryController {

    @Autowired
    DataLayer dataLayer;

    @GetMapping(value = "/getBorrowers")
    public List<User> getBorrowers(){
        return dataLayer.getBorrowers();
    }

    @GetMapping(value = "/getPassiveUsers")
    public List<User> getPassiveUsers(){
        return dataLayer.getPassiveUsers();
    }

    @GetMapping(value = "/getBorrowersByDate/{dateStr}")
    public List<User> getBorrowersByDate(@PathVariable String dateStr){
        return dataLayer.getBorrowersByDate(dateStr);
    }

    @GetMapping(value = "/getAvailableBooks")
    public List<Book> getAvailableBooks(){
        return dataLayer.getAvailableBooks();
    }
}
