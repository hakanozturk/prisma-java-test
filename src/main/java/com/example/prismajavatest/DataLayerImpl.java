package com.example.prismajavatest;

import com.example.prismajavatest.entity.Book;
import com.example.prismajavatest.entity.BorrowedBook;
import com.example.prismajavatest.entity.User;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service
public class DataLayerImpl implements DataLayer {

    @Autowired
    DataUtil dataUtil;

    @Override
    public List<User> getBorrowers() {
        List<User> allUsers = dataUtil.getUsers();
        List<BorrowedBook> borrowedBooks = dataUtil.getBorrowedBooks();
        if(allUsers == null || borrowedBooks == null){
            return new ArrayList<User>();
        }
        List<User> borrowers =  allUsers.stream().filter(user -> {
           return borrowedBooks.stream().anyMatch(borrowedBook -> {
              return borrowedBook.getBorrower().equals(User.getUserLastAndFirstName(user));
            });
        }).collect(Collectors.toList());

        return borrowers;
    }

    @Override
    public List<User> getPassiveUsers() {
        List<User> allUsers = dataUtil.getUsers();
        ;
        List<User> borrowers =  allUsers.stream().filter(this::userHasCheckedOutBook).collect(Collectors.toList());

        return borrowers;
    }

    private Boolean userHasCheckedOutBook(User user){
        if(!checkIfUserIsActive(user)){
            return false;
        }
        List<BorrowedBook> borrowedBooks = dataUtil.getBorrowedBooks();
        for(BorrowedBook borrowedBook: borrowedBooks){
            if (borrowedBook.getBorrower().equals(User.getUserLastAndFirstName(user))){
                if (!checkIfBookIsAvailable(borrowedBook)){
                    return false;
                }
            }
        }
        return true;
    }

    private Boolean checkIfUserIsActive(User user){
        return user.getMemberUntil() == null  || user.getMemberUntil().isAfter(LocalDate.now());
    }
    //borrowedAt > now > borrowedUntil
    private Boolean checkIfBookIsAvailable(BorrowedBook borrowedBook){
        return !(LocalDate.now().isAfter(borrowedBook.getBorrowedAt()) && LocalDate.now().isBefore(borrowedBook.getBorrowedUntil()));
    }

    @Override
    public List<User> getBorrowersByDate(String dateStr) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");

        LocalDate date = LocalDate.parse(dateStr, formatter);

        List<User> allUsers = dataUtil.getUsers();
        List<BorrowedBook> borrowedBooks = dataUtil.getBorrowedBooks();
        List<User> borrowers =  allUsers.stream().filter(user -> {
            return borrowedBooks.stream().anyMatch(borrowedBook -> {
                return borrowedBook.getBorrower().equals(User.getUserLastAndFirstName(user)) && borrowedBook.getBorrowedAt().equals(date);
            });
        }).collect(Collectors.toList());

        return borrowers;
    }

    @Override
    public List<Book> getBooksByUserAndDate() {
        return null;
    }

    @Override
    public List<Book> getAvailableBooks() {
        List<Book> allBooks = dataUtil.getBooks();
        Set<String> availableBookSet = dataUtil.getBorrowedBooks().stream().filter(borrowedBook -> !checkIfBookIsAvailable(borrowedBook)).map(BorrowedBook::getBook).collect(Collectors.toSet());
        List<Book> availableBooks = allBooks.stream().filter(book -> !availableBookSet.contains(book.getTitle())).collect(Collectors.toList());
        return availableBooks;
    }
}
