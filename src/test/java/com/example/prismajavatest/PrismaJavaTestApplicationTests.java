package com.example.prismajavatest;

import com.example.prismajavatest.entity.Book;
import com.example.prismajavatest.entity.BorrowedBook;
import com.example.prismajavatest.entity.User;
import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

@SpringBootTest
class PrismaJavaTestApplicationTests {

	private DataUtil dataUtil = Mockito.mock(DataUtil.class);

	@Before
	public void setUp(){}

	@Test
	void getBorrowersNullCheck(){
		Mockito.when(dataUtil.getUsers()).thenReturn(null);
		Mockito.when(dataUtil.getBooks()).thenReturn(null);
		Mockito.when(dataUtil.getBorrowedBooks()).thenReturn(null);
		DataLayer dataLayer = new DataLayerImpl(dataUtil);
		Assertions.assertTrue(dataLayer.getBorrowers().isEmpty());
	}

	@Test
	void getBorrowers(){
		List<User> users = Collections.singletonList(new User(1,"b","a",LocalDate.now(),LocalDate.now(),""));
		List<Book> books = Collections.singletonList(new Book(1,"","","",""));
		List<BorrowedBook> borrowedBooks = Collections.singletonList(new BorrowedBook(User.getUserLastAndFirstName(users.get(0)),"", LocalDate.now(), LocalDate.now()));
		Mockito.when(dataUtil.getUsers()).thenReturn(users);
		Mockito.when(dataUtil.getBooks()).thenReturn(books);
		Mockito.when(dataUtil.getBorrowedBooks()).thenReturn(borrowedBooks);
		DataLayer dataLayer = new DataLayerImpl(dataUtil);
		List<User> borrowers = dataLayer.getBorrowers();
		Assertions.assertFalse(borrowers.isEmpty());
		Assertions.assertEquals(users.get(0), borrowers.get(0));
	}


}
